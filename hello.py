# print("entry1")


import unl_core_rust_python_bindings as u

geocoords = u.decode("tester")
print(geocoords)

# lat, lon
print(u.encode(geocoords[0], geocoords[1]))

# print(u.decode("t0v79p2ch"))

polyh = "yyA2OSqEXgAFoCulvLTnDlDqDzoInRBFwF38kU0L8HQW/A=="
route = u.decode_polyhash(polyh)

print(route)

lat_lon_route = map(u.decode, route)

print(list(lat_lon_route))

print(u.encode_polyhash(route))

